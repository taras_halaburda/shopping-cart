FROM node:14

WORKDIR /var/www/app

COPY . /var/www/app

RUN npm install -g @angular/cli@11.2.9
RUN npm install 

EXPOSE 4200

CMD npm run start