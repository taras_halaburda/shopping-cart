import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class SharedService {

  public cartItems = [];
  public products = new Subject();

  constructor() {
    let cartItems = JSON.parse(localStorage.getItem('cartItems'));

    if (cartItems && cartItems.length) {
      this.cartItems = cartItems;
    }
  }

  getProducts(): Observable<any> {
    return this.products.asObservable();
  }

  setProducts(products) {
    this.cartItems.push(...products);
    this.products.next(products);
  }

  // Add single product to the cart
  addProductToCart(product) {
    let selected = this.cartItems.filter((cartItem) => {
      return product.id == cartItem.id;
    })

    if (!selected.length) {
      product.selected = true
      this.cartItems.push(product);
      this.products.next(this.cartItems);
      localStorage.setItem('cartItems', JSON.stringify(this.cartItems))
    }
  }

  // Remove single product from the cart
  removeProductFromCart(productId) {
    this.cartItems.map((item, index) => {
      if (item.id === productId) {
        this.cartItems.splice(index, 1);
      }
    });

    localStorage.setItem('cartItems', JSON.stringify(this.cartItems));

    // Update Observable value
    this.products.next(this.cartItems);
  }

  deleteAllFromCart() {
    this.cartItems.length = 0;
    localStorage.removeItem('cartItems');
    this.products.next(this.cartItems);
  }

  getTotalPrice() {
    let total = 0;

    this.cartItems.map(item => {
      total += item.price;
    });

    return total
  }
}