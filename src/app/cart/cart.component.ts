import { Component, OnInit } from '@angular/core';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  public cartItems;
  private totalAmmount;

  constructor(private sharedService: SharedService) { }

  ngOnInit() {
    this.cartItems = this.sharedService.cartItems;
    this.totalAmmount = this.sharedService.getTotalPrice();

    this.sharedService.getProducts().subscribe(data => {
      this.cartItems = data;
      this.totalAmmount = this.sharedService.getTotalPrice();
    });
  }

  removeItemFromCart(productId) {
    this.sharedService.removeProductFromCart(productId);
  }

  emptyCart() {
    if (confirm("Are you sure to delete")) {
      this.sharedService.deleteAllFromCart();
    }
  }
}