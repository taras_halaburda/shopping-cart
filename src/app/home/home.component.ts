import { Component, OnInit } from '@angular/core';
import { MainService } from '../services/main.service';
import { SharedService } from '../services/shared.service';
import { Injectable } from '@angular/core';

@Injectable()

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  products: any = [];
  private singleProduct;

  constructor(private mainService: MainService, private sharedService: SharedService) { }

  ngOnInit() {
    this.mainService.getJSON().subscribe(response => {
      this.products = response.data
      this.products.map((item) => {
        item.selected = !!this.sharedService.cartItems.filter((cartItem) => {
          return item.id == cartItem.id;
        }).length
        return item;
      })
    })
  }

  // Add item in cart on Button click
  addToCart(product) {
    this.sharedService.addProductToCart(product);
  }
}
