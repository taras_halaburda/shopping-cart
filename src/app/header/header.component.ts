import { Component, OnInit } from '@angular/core';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public cartProductCount: number = 0;
  constructor(private sharedService: SharedService) { }

  ngOnInit() {
    this.cartProductCount = this.sharedService.cartItems.length;
    this.sharedService.getProducts().subscribe(data => {
      this.cartProductCount = data.length;
    })
  }
}
